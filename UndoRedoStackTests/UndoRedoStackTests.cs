﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UndoRedoStackTests
{
    [TestClass]
    public class UndoRedoStackTests
    {
        [TestMethod]
        public void CanCreateStack()
        {
            UndoRedoStack<string> stack = new UndoRedoStack<string>();
            Assert.IsNotNull(stack);
        }

        [TestMethod]
        public void CanCreateWithCustomSizeStack()
        {
            int customInitialSize = 1000;
            UndoRedoStack<string> stack = new UndoRedoStack<string>(customInitialSize);
            Assert.IsNotNull(stack);
        }

        [TestMethod]
        public void IsCreatedStackEmpty()
        {
            UndoRedoStack<string> stack = new UndoRedoStack<string>();
            Assert.AreEqual(0, stack.ActualSize);
        }

        [TestMethod]
        public void CanAddToStack()
        {
            UndoRedoStack<string> stack = new UndoRedoStack<string>();
            stack.Add("cmd1");
            Assert.AreEqual(1, stack.ActualSize);
        }

        [TestMethod]
        public void CanUndoFromStack()
        {
            UndoRedoStack<string> stack = new UndoRedoStack<string>();
            string newCommand = "cmd1";
            stack.Add(newCommand);
            var undoResult = stack.Undo();
            Assert.AreEqual(undoResult, newCommand);
        }

        [TestMethod]
        public void CanUndoProperCommandFromStack()
        {
            UndoRedoStack<string> stack = new UndoRedoStack<string>();
            string newCommand1 = "cmd1";
            string newCommand2 = "cmd1";
            stack.Add(newCommand1);
            stack.Add(newCommand2);
            var undoResult = stack.Undo();
            Assert.AreEqual(undoResult, newCommand2);
        }


        [TestMethod]
        public void IsActualSizeProperyDecreased()
        {
            UndoRedoStack<string> stack = new UndoRedoStack<string>();
            string newCommand = "cmd1";
            stack.Add(newCommand);
            stack.Undo();
            Assert.AreEqual(0, stack.ActualSize);
        }

        [TestMethod]
        [ExpectedException(typeof(StackEmptyException), "Stack empty.")]
        public void ThrowExceptionIfUndoCacheEmpty()
        {
            UndoRedoStack<string> stack = new UndoRedoStack<string>();
            stack.Undo();
        }

        [TestMethod]
        public void CanRedoFromStack()
        {
            UndoRedoStack<string> stack = new UndoRedoStack<string>();
            string firstCommand = "cmd1";
            string redoCommand = "cmd2";
            stack.Add(firstCommand);
            stack.Add(redoCommand);
            stack.Undo();
            var redoResult = stack.Redo();
            Assert.AreEqual(redoCommand, redoResult);
        }



        [TestMethod]
        [ExpectedException(typeof(RedoEmptyException), "Stack empty.")]
        public void ThrowExceptionIfNoRedoPossible()
        {
            UndoRedoStack<string> stack = new UndoRedoStack<string>();
            stack.Redo();
        }

        [TestMethod]
        [ExpectedException(typeof(RedoEmptyException), "Stack empty.")]
        public void IsStackProperyHandledWithDoubleRedo()
        {
            UndoRedoStack<string> stack = new UndoRedoStack<string>();
            string firstCommand = "cmd1";
            string redoCommand = "cmd2";
            stack.Add(firstCommand);
            stack.Add(redoCommand);
            stack.Undo();
            stack.Redo();
            stack.Redo();
        }


        //If we add command 1, then command 2, then undo 2, then add command 3, we can undo 3. 
        //When we undo 3, we can then redo 3 or undo 1. 
        //When we undo 1, we can redo 1. If we redo 1, we can then undo 1 or redo 3.
        [TestMethod]
        public void CanAdd1Add2Undo2Add3Undo3BeProperlyHandled()
        {
            UndoRedoStack<string> stack = new UndoRedoStack<string>();
            string command1 = "cmd1";
            string command2 = "cmd2";
            string command3 = "cmd3";
            stack.Add(command1);
            stack.Add(command2);
            stack.Undo();
            stack.Add(command3);
            Assert.AreEqual(command3, stack.Undo());
        }


        [TestMethod]
        public void CanAdd1Add2Undo2Add3Undo3Redo3BeProperlyHandled()
        {
            UndoRedoStack<string> stack = new UndoRedoStack<string>();
            string command1 = "cmd1";
            string command2 = "cmd2";
            string command3 = "cmd3";
            stack.Add(command1);
            stack.Add(command2);
            stack.Undo();
            stack.Add(command3);
            stack.Undo();
            Assert.AreEqual(command3, stack.Redo());
        }

        [TestMethod]
        public void CanAdd1Add2Undo2Add3Undo3Undo1BeProperlyHandled()
        {
            UndoRedoStack<string> stack = new UndoRedoStack<string>();
            string command1 = "cmd1";
            string command2 = "cmd2";
            string command3 = "cmd3";
            stack.Add(command1);
            stack.Add(command2);
            stack.Undo();
            stack.Add(command3);
            stack.Undo();
            Assert.AreEqual(command1, stack.Undo());
        }

        [TestMethod]
        public void CanAdd1Add2Undo2Add3Undo3Undo1Redo1BeProperlyHandled()
        {
            UndoRedoStack<string> stack = new UndoRedoStack<string>();
            string command1 = "cmd1";
            string command2 = "cmd2";
            string command3 = "cmd3";
            stack.Add(command1);
            stack.Add(command2);
            stack.Undo();
            stack.Add(command3);
            stack.Undo();
            stack.Undo();
            Assert.AreEqual(command1, stack.Redo());
        }

        [TestMethod]
        public void CanAdd1Add2Undo2Add3Undo3Undo1Redo3BeProperlyHandled()
        {
            UndoRedoStack<string> stack = new UndoRedoStack<string>();
            string command1 = "cmd1";
            string command2 = "cmd2";
            string command3 = "cmd3";
            stack.Add(command1);
            stack.Add(command2);
            stack.Undo();
            stack.Add(command3);
            stack.Undo();
            Assert.AreEqual(command3, stack.Redo());
        }

    }
}
