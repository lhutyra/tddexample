﻿using System;
using System.Runtime.Serialization;

namespace UndoRedoStackTests
{
    [Serializable]
    public class RedoEmptyException : Exception
    {
        public RedoEmptyException()
        {
        }

        public RedoEmptyException(string message) : base(message)
        {
        }

        public RedoEmptyException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected RedoEmptyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}