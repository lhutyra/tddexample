﻿using System;
using System.Collections.Generic;

namespace UndoRedoStackTests
{
    /// <summary>
    /// Implementation of Undo Redo Stack 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class UndoRedoStack<T>
    {
        private T[] internalStack;
        private T[] redoList;
        private const int defaultCapacity = 10;
        private int stackPointer = 0;
        private int redoStackPointer = 0;
        private T redoCache;

        /// <summary>
        /// Default constructor
        /// </summary>
        public UndoRedoStack()
        {
            internalStack = new T[defaultCapacity];
            redoList = new T[defaultCapacity];
            redoCache = default(T);
        }

        /// <summary>
        /// Custom constructor
        /// </summary>
        /// <param name="customCapacity">default size</param>
        public UndoRedoStack(int customCapacity)
        {
            internalStack = new T[customCapacity];
            redoCache = default(T);
        }

        /// <summary>
        /// Actual stack size
        /// </summary>
        public int ActualSize
        {
            get
            {
                return stackPointer;
            }
        }

        /// <summary>
        /// Add new value to stack
        /// </summary>
        /// <param name="newValue"></param>
        public void Add(T newValue)
        {
            internalStack[stackPointer + 1] = newValue;
            if (stackPointer == internalStack.Length)
            {
                resize(internalStack.Length * 2,ref internalStack);
            }
            stackPointer++;
        }

        /// <summary>
        /// Undo last command
        /// </summary>
        /// <returns>Return last command</returns>
        public T Undo()
        {
            if (stackPointer > 0)
            {
                var undoAux = internalStack[stackPointer--];
                if (redoStackPointer < redoList.Length)
                {
                    redoList[++redoStackPointer] = undoAux;
                }
                else
                {
                    resize(redoList.Length * 2, ref redoList);
                }
                return undoAux;
            }
            else
            {
                throw new StackEmptyException("Stack empty");
            }
        }

        /// <summary>
        /// Redo command which has been undone
        /// </summary>
        /// <returns>Previously undone command</returns>
        public T Redo()
        {
            if (redoStackPointer > 0)
            {
                redoCache = redoList[redoStackPointer--];
                return redoCache;
            }
            else
            {
                throw new RedoEmptyException("Invalid operation redo");
            }
        }


        /// <summary>
        /// Internal helper method
        /// </summary>
        /// <param name="newSize">new array size</param>
        private void resize(int newSize, ref T[] ordignalArray)
        {
            T[] aux = new T[newSize];
            for (int i = 0; i < stackPointer; i++)
            {
                aux[i] = internalStack[i];
            }
            ordignalArray = aux;
        }

       
    }
}